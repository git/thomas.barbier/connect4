import unittest
from parameterized import parameterized
from ..src.player import Players
# from player import Players

class TestPlayer(unittest.TestCase):
    @parameterized.expand([
        ("joe", "jonny", True),
        (1, "jonny", False),
        ("joe", 15, False),
    ])
    def testString(self, name1, name2, no_exception):
        try:
            players = Players(name1=name1, name2=name2)
            self.assertTrue(no_exception)
        except:
            self.assertFalse(no_exception)

if __name__=='__main__':
    unittest.main()