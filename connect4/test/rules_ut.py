import unittest
from parameterized import parameterized

from ..src.rules import Rules
from ..src.board import Board

class TestRules(unittest.TestCase):
	@parameterized.expand([
		(Board(6, 7), True),
		(Board(10, 10), False),
	])
	def test_check_board_size(self, board, no_exception):
		rules = Rules()

		if no_exception:
			self.assertTrue(rules.check_board_size(board))
		else:
			self.assertFalse(rules.check_board_size(board))

if __name__ == "__main__":
	unittest.main()