import unittest
from ..src.board import Board
from parameterized import parameterized

class TestBoard(unittest.TestCase):
    @parameterized.expand([
        (8, 8, True),
        ("8", "8", False),
    ])
    def testString(self, nbrow, nbcol, no_exception):
        try:
            board = Board(nbrow=nbrow, nbcol=nbcol)
        except:
            self.assertFalse(no_exception)
    
    @parameterized.expand([
        (0, 1, True),
        (0, 2, True),
        (0, 0, False),
        (10, 1, False),
        (10, 2, False),
    ])
    def testInsertPiece(self, col, player_id, no_exception):
        board = Board(8, 8)

        if no_exception:
            self.assertTrue(board.insert_piece_into_column(col, player_id))
        else:
            self.assertFalse(board.insert_piece_into_column(col, player_id))

if __name__=='__main__':
    unittest.main()