import numpy as np

class Board:
    def __init__(self, nbrow, nbcol):
        self.nbrow = nbrow
        self.nbcol = nbcol
        self.board = np.zeros((nbrow, nbcol))
    
    def check_boundaries(self, row, col):
        try:
            self.board[row, col]
            return True
        except:
            return False

    def remove_piece(self, row, col):
        self.board[row, col] = 0
    
    def insert_piece(self, row, col, player_id):
        self.board[row, col] = player_id
    
    def insert_piece_into_column(self, col, player_id):
        if player_id <= 0:
            return False
        if not self.check_boundaries(0, col):
            return False

        indices = np.where(self.board[:, col] == 0)[0]
        if indices.size > 0:
            row = indices[0]
            self.insert_piece(row, col, player_id)
            return True
        else:
            return False
        