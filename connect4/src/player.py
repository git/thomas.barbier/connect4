class Players:

    def __init__(self,name1,name2):
        
        assert type(name1) == str and type(name2)==str, "names must be a string"
        self.id1 = 1
        self.id2 = 2
        self.name1 = name1
        self.name2 = name2